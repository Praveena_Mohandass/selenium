package bl.framework.Excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellAlignment;

public class readExcel {

	static Object[][]  data ;
	public static Object[] readExcel(String Datasheetname)  {
		// TODO Auto-generated method stub
 
		try {
			XSSFWorkbook workbook = new XSSFWorkbook("./Data/"+Datasheetname+".xlsx");
			
			XSSFSheet sheet = workbook.getSheet("Sheet1");
			
			int RowCount = sheet.getLastRowNum();
			
			System.out.println("Row count -" + RowCount);
			
			int columnCount = sheet.getRow(0).getLastCellNum();
			
			System.out.println("Column count-" + columnCount);
			
			data = new Object[RowCount][columnCount];
			
			for(int i= 1; i<= RowCount ;i++) {
				
				XSSFRow row = sheet.getRow(i);
				
				for(int j=0;j<columnCount;j++) {
					
					XSSFCell column= row.getCell(j);
					
					//String value = column.getStringCellValue();
					
					data[i-1][j] = column.getStringCellValue();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
		
	}

}
