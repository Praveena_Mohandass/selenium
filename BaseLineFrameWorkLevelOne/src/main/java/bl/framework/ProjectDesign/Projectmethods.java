package bl.framework.ProjectDesign;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import  bl.framework.reports.baseReports;
import bl.framework.Excel.readExcel;
import bl.framework.api.SeleniumBase;

public class Projectmethods extends SeleniumBase{
	
	public String testcaseName,testdesc,author,category,datasheetname;
	@BeforeSuite(groups="Common")	
	public void beforesuite() {
		startreport();
	}
	
	@AfterSuite(groups="Common")	
	
	public void afterSuite() {
		endReport();
	}
	
	@BeforeClass(groups="Common")	
	
	public void beforeClass() {
	
		initializetest(testcaseName,testdesc,category,author);
	}
	
	@BeforeMethod(groups="Common")	
	public void login () {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
	    WebElement eleLink = locateElement("Linktext","CRM/SFA");
		click(eleLink);
	}

	@AfterMethod(groups="Common")
	
	public void aftecloseapp() {
		
		close();
	}
	
	@DataProvider(name="readExcel")
	public Object[] getData() {
		
		return readExcel.readExcel(datasheetname);
	}
	
	}

