package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.microsoft.schemas.compatibility.*;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import bl.framework.reports.baseReports;
public class SeleniumBase  extends baseReports implements Browser, Element {

	public RemoteWebDriver driver;
	public int i =1;
	
	Alert alert;
	@Override
	public void startApp(String url) {
		 try {
			System.setProperty("webdriver.chrome.driver", "./driver/cromedriver.exe");
			  driver = new ChromeDriver();
			 driver.get(url);
			 logStep("The url is loaded","Pass");
			 
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
		}
		 
		 catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				logStep("Java null pointer exception","Fail");
			}
		 catch (WebDriverException e) {
				// TODO Auto-generated catch block
				logStep("Java null pointer exception","Fail");
		 }
      finally{
	  takeSnap();
  }

	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver(); 
			}
			driver.manage().window().maximize();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The browser "+browser+" launched successfully");
			takeSnap();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
		}
	 catch (WebDriverException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
  finally{
  takeSnap();
	}
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch (locatorType) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "xpath": return driver.findElementByXPath(value);
			case "Linktext": return driver.findElementByLinkText(value);
			case "tagname" : return driver.findElement(By.tagName(value));
			default:
				break;
			}
			
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (WebDriverException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
			 
	 }
  finally{
  takeSnap();
	}
	return null;
	}

	@Override
	public WebElement locateElement(String value) {
		// locate by id
		try {
		return driver.findElementById(value) ;
	}
		catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (WebDriverException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
  finally{
  takeSnap();
	}
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		// All the matching elements of a particular locator type
		
		try {
			switch (type) {
			case "id": return driver.findElementsById(value);
			case "name": return driver.findElementsByName(value);
			case "class": return driver.findElementsByClassName(value);
			case "xpath": return driver.findElementsByXPath(value);
			case "Linktext": return driver.findElementsByLinkText(value);
			case "tagname" : return driver.findElements(By.tagName(value));
			default:
				break;
			}
			return null;
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (WebDriverException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
  finally{
  takeSnap();
	}
		return null;
	}

	@Override
	public void switchToAlert() {
		// Switch the control fro web page to alert
		try {
		alert=driver.switchTo().alert();

	}
	catch (NoAlertPresentException e) {
		// TODO Auto-generated catch block
		logStep("Java null pointer exception","Fail");
 }
		catch (UnhandledAlertException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
}

	@Override
	public void acceptAlert() {
		// To click ok on a alert prompt
		try {
		alert.accept();
		
	}
		catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
			catch (UnhandledAlertException e) {
				// TODO Auto-generated catch block
				logStep("Java null pointer exception","Fail");
		 }

	}

	@Override
	public void dismissAlert() {
		// To click cancel on a alert prompt
		try {
		alert.dismiss();
		}
		catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
			catch (UnhandledAlertException e) {
				// TODO Auto-generated catch block
				logStep("Java null pointer exception","Fail");
		 }
	}

	@Override
	public String getAlertText() {
		// To get the text on a alert
		try {
		return  alert.getText();
	}
		catch (NoAlertPresentException e) {
		// TODO Auto-generated catch block
		logStep("Java null pointer exception","Fail");
		
 }
		catch (UnhandledAlertException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
		return null;
	}

	@Override
	public void typeAlert(String data) {
		// To give text in alert prompt
		try {
			alert.sendKeys(data);
			logStep("The alert text is entered","Pass");
		}
		catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
		}
			
	}

	@Override
	public void switchToWindow(int index) {
		// To switch the control to a window by index value
		try {
    Set<String> win= driver.getWindowHandles();
    List<String> wins = new ArrayList();
	wins.addAll(win);
	driver.switchTo().window(wins.get(index));
		}
		catch (NoSuchWindowException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
			catch (WebDriverException e) {
				// TODO Auto-generated catch block
				logStep("Java null pointer exception","Fail");
		 }
	
	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub
		
		try {
			  Set<String> win= driver.getWindowHandles();
		    List<String> wins = new ArrayList();
			wins.addAll(win);
		driver.switchTo().window(title);
		}
		catch (NoSuchWindowException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
			catch (WebDriverException e) {
				// TODO Auto-generated catch block
				logStep("Java null pointer exception","Fail");
		 }

	}

	@Override
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(index);

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().frame(ele);
		}
		catch (NoSuchFrameException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
			catch (WebDriverException e) {
				// TODO Auto-generated catch block
				logStep("Java null pointer exception","Fail");
		 }
		
	}

	@Override
	public void switchToFrame(String idOrName) {
		// TODO Auto-generated method stub
     try {
		driver.switchTo().frame(idOrName);
     }
     catch (NoSuchFrameException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
			catch (WebDriverException e) {
				// TODO Auto-generated catch block
				logStep("Java null pointer exception","Fail");
		 }
	}

	@Override
	public void defaultContent() {
		// TODO Auto-generated method stub
		try {
     driver.switchTo().defaultContent();
	}
     catch (NoSuchFrameException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
			catch (WebDriverException e) {
				// TODO Auto-generated catch block
				logStep("Java null pointer exception","Fail");
		 }
	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		// TODO Auto-generated method stub
		try {
		if(driver.getTitle().equals(title))
			return true;
			else {
				
		return false;
	}
		}
		catch (NullPointerException e) {
			// TODO Auto-generated catch block
			logStep("Java null pointer exception","Fail");
	 }
		return false;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
  driver.close();
	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub
    driver.quit();
	}

	@Override
	public void click(WebElement ele) {
		try {
		ele.click();
		System.out.println("The element "+ele+" clicked successfully");
		takeSnap();
		}
		catch (java.util.NoSuchElementException e) {
			e.printStackTrace();
		}
	}
	
	public void clickwithoutsnap(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+" clicked successfully");
		
		
	}

	@Override
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub
		ele.sendKeys(data);
		
		
	}

	@Override
	public void clear(WebElement ele) {
		// TODO Auto-generated method stub
		ele.clear();
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		ele.clear();
		ele.sendKeys(data); 
		System.out.println("The data "+data+" entered successfully");
		takeSnap();
	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		return ele.getText();
		
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return ele.getText();
		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
     Select sel = new Select(ele);
		
		sel.selectByVisibleText(value);
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
      Select sel = new Select(ele);
		
		sel.selectByIndex(index);
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		
		Select sel = new Select(ele);
		
		sel.selectByValue(value);
	
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

		if(ele.getText().equals(expectedText)) {
			System.out.println("The element contains the expected text");
			return true;
		}
		else {
			System.out.println("The element does not contain the expected text");
			return false;
		}
		
	
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		
		if(ele.getText().contains(expectedText)) {
			System.out.println("The element contains the partial expected text");
			return true;
		}
		else {
			System.out.println("The element does not contain the partial expected text");
			return false;
		}
		
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
		if(ele.getAttribute(attribute).equals(value)) {
			System.out.println("The element contains the  attriute value");
			return true;
		}
		else {
			System.out.println("The element does not contain the  attriute value");
			return false;
		}
		
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
		if(ele.getAttribute(attribute).contains(value)) {
			System.out.println("The element contains the partial attriute value");
		}
		else {
			System.out.println("The element does not contain the partial attriute value");
		}
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isDisplayed()== true) {
			System.out.println("The element is displayed");
			return true;
		}
		
		else {
			
			System.out.println("The element is not displayed");
			return false;
		}
		
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isDisplayed()== false){
			System.out.println("The element is disappeared");
			return true;
		}
else {
			
			System.out.println("The element is not disappeared");
			return false;
		}
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isEnabled()== true) {
			System.out.println("The element is Enabled");
			return true;
		}
		
		else {
			
			System.out.println("The element is not Enabled");
			return false;
		}
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isSelected()== true) {
			System.out.println("The element is selected");
			return true;
		}
		
		else {
			
			System.out.println("The element is not selected");
			return false;
		}
	}

	

}
