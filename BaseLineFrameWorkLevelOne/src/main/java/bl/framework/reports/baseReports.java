package bl.framework.reports;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import bl.framework.api.SeleniumBase;


public class baseReports {

	 static ExtentHtmlReporter html;
	
	static ExtentReports report;
	
	ExtentTest test;
	
	public void startreport() {
		
		html= new ExtentHtmlReporter("./Reports/report.html");
		
		report = new ExtentReports();
		
		html.setAppendExisting(true);
		
		report.attachReporter(html);
	}
	
	 public void initializetest(String tcname,String testdec,String category,String author) {
		
		test = report.createTest(tcname, testdec);
		
		test.assignCategory(category);
		
		test.assignAuthor(author);
	 }
		
		public void logStep(String des, String status) {
			if (status.equalsIgnoreCase("Pass")) {
				test.pass(des);
				}
			else if(status.equalsIgnoreCase("Fail")) {
				test.fail(des);
			}
			else if (status.equalsIgnoreCase("warning")) {
				test.warning(des);
			}
		}
		
		
//		try {
//			test.fail("Logged in successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img11.png").build());
//			
//			test.pass("Logged in successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img1.png").build());
//			
//			test.warning("Logged in successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img1.png").build());
//			
//			test.skip("Logged in successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img1.png").build());
//			
//					
//			
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		public void endReport() {
			
	report.flush();
	}
}
