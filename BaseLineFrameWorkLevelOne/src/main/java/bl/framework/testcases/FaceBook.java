package bl.framework.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FaceBook {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
          ChromeOptions op=new ChromeOptions();
	     
	     op.addArguments("--disable-notifications");
	     
	      ChromeDriver driver = new ChromeDriver(op);
	     
	     driver.manage().window().maximize();
	     
	     driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	     
	     //Launch the URL
	     
	     driver.get("https://www.facebook.com/");
	     
	     // Enter the username
	     
	     driver.findElementById("email").sendKeys("praveena2402@gmail.com");
	     
	     // Enter the password
	     
	     driver.findElementById("pass").sendKeys("Praveenamohan");
	     
	     driver.findElementByXPath("//input[@data-testid='royal_login_button']").click();
	     
	     //Enter test leaf in search box
	     
	     String str ="TestLeaf";
	     
	     driver.findElementByXPath("//input[@placeholder ='Search']").sendKeys(str);
	     
	     
	     
	     //click on search button
	     
	    
	     
	     driver.findElementByXPath("//button[@data-testid='facebar_search_button']/i").click();
	     
	     //Click on places
	     
           WebDriverWait wait = new WebDriverWait(driver,300);
	     
	     wait.until(ExpectedConditions.elementToBeClickable (driver.findElementByXPath("//div[text()='Places']")));
	     
	    driver.findElementByXPath("//div[text()='Places']").click();
	    
	    //Find the text Test leaf under places
	     
	     Thread.sleep(2000);
	    
	   
	    
	   String str2= driver.findElementByXPath("//a[@href='https://www.facebook.com/TestleafSolutionsIncChennai/']").getText();
	   
	   System.out.println(str2);
//	    
//	   if(str2.equalsIgnoreCase(str)){
//		   System.out.println("Test leaf is displayed");
//	   }
//	     
	   //Capture the text of like button
	     
	     String str3 =driver.findElementByXPath("//button[@data-testid='search_like_button_test_id'][1]").getText();
	   
	   if(driver.findElementByXPath("//button[@data-testid='search_like_button_test_id'][1]").getText().equalsIgnoreCase("Like")) {
		   
		   driver.findElementByXPath("//button[@data-testid='search_like_button_test_id'][1]").click();
		   
		   System.out.println("Like");
	   }
	   
	   else if (driver.findElementByXPath("//button[@data-testid='search_like_button_test_id'][1]").getText().equalsIgnoreCase("Liked")){
	   		 
		   System.out.println("Test Leaf page is already liked");
		   
	   }
	   
	   driver.findElementByXPath("//span[@data-bt='{\"ct\":\"place_name\"}']").click();
	   
	   String countLikes = driver.findElementByXPath("//div[text()='7,111 people like this']").getText();
	   
	   System.out.println(countLikes);
	}

}
