package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC001_LoginAndLogout extends SeleniumBase{

	//@Test(groups="Reg")
	
	@Parameters({"url","username","password"})
	@Test
	public void login(String url, String username, String pwd) {
		startApp("chrome", url);
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, username); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, pwd); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		 WebElement eleLink = locateElement("Linktext","CRM/SFA");
			click(eleLink);
//		WebElement eleLogout = locateElement("class", "decorativeSubmit");
//		click(eleLogout);
	}
}








