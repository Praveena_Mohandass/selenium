package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bl.framework.ProjectDesign.Projectmethods;

public class TC002_Create_Leads extends Projectmethods{
//@BeforeTest(groups="Common")
	@BeforeTest
	public void SetData() {
		testcaseName ="TC002_Create_Leads";
		testdesc="Edit the leads";
	    author="Praveena";
		category="Regression";
		datasheetname="TC002_Create_Leads";
		
	}
	
//@Test(groups="Sanity")
	 
	@Test(dataProvider="readExcel")
	public void createLeads(String cname,String firstname,String lastname) {
		
		
		WebElement eleCreateLead = locateElement("Linktext", "Create Lead");
		
		click(eleCreateLead);
		
		WebElement elecompanyName = locateElement("id","createLeadForm_companyName");
		
		clearAndType(elecompanyName, cname);		
		
		WebElement elefirstName = locateElement("id", "createLeadForm_firstName");
		
		clearAndType(elefirstName,firstname);
		
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		
		clearAndType(eleLastName, lastname);
		
		WebElement eleSubmit = locateElement("name","submitButton");
		
		click(eleSubmit);
		
	}

//	@DataProvider (name= "testData1")
//	public void testData1() {
//		
//		String[][] leads = new String[2][3];
//		
//		leads[0][0]="Capgemini";
//		
//		leads[0][1]="Praveena";
//		
//		leads[0][2]="Mohandass";
//		
//		leads[1][0]="Infosys";
//		
//		leads[1][1]="Preethi";
//		
//		leads[1][2]="Mohandass";
//		
//	}
//	
//	@DataProvider (name= "testData2")
//	public void testData2() {
//		
//		String[][] leads1 = new String[2][3];
//		
//		leads1[0][0]="Capgemini";
//		
//		leads1[0][1]="Praveena";
//		
//		leads1[0][2]="Mohandass";
//		
//		leads1[1][0]="Infosys";
//		
//		leads1[1][1]="Preethi";
//		
//		leads1[1][2]="Mohandass";
//		
//	}
	}
