package bl.framework.testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.ProjectDesign.Projectmethods;


public class TC_003_DuplicateLeads extends Projectmethods{
@BeforeTest
	
	public void SetData() {
		testcaseName ="TC_004_EditLead";
		testdesc="Edit the leads";
	    author="Praveena";
		category="Regression";
	}
	
	@Test
	
	 public void duplicateLeads() throws InterruptedException {
	
		
		
		WebElement leads = locateElement("Linktext", "Leads");
		click(leads);
		WebElement findLeads = locateElement("xpath", "//a[@href='/crmsfa/control/findLeads']");
		click(findLeads);
		WebElement email = locateElement("Linktext","Email");
		click(email);
		clearAndType(driver.findElementByName("emailAddress"), "abcd@gmail.com");
		WebElement findLead = locateElement("xpath","//button[text()='Find Leads']");
		click(findLead);
		WebElement leadTable=locateElement("xpath","//table[@class='x-grid3-row-table']");
		WebElement row = locateElement("tagname", "tr");
		WebElement cols = locateElement("tagname", "td");
		WebElement firstLeadName = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-firstName']");
		
		String firstname =getElementText(firstLeadName);
		System.out.println("the first lead name is" +firstname);
		
		Thread.sleep(2000);
		//WebElement firstid=cols.findElement(By.tagName("a"));
		cols.findElement(By.xpath("(//a[@class='linktext'])[4]")).click();
		WebElement duplicateLead = locateElement("xpath","//a[text()='Duplicate Lead']");
		click(duplicateLead);
		String title = locateElement("xpath","//div[text()='Duplicate Lead']").getText();
		if(verifyTitle(title)==true) {
			System.out.println("The title is verified as Duplicate lead");
		}
		else {
			System.out.println("The title is not verified as Duplicate lead");
		}
		click(locateElement("class", "smallSubmit"));
		
		String name = locateElement("id", "viewLead_firstName_sp").getText();
		
		System.out.println("Duplicate name " +name);
		
		if(name.equals(firstname)) {
			System.out.println("The lead name is duplicate");
		}
		else
			System.out.println("The lead name is not duplicate");
		
	 }

}
