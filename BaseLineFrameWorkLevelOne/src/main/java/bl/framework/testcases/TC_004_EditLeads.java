package bl.framework.testcases;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.ProjectDesign.Projectmethods;
import bl.framework.api.SeleniumBase;


public class TC_004_EditLeads extends Projectmethods {
	
	@BeforeTest(groups="Common")
	
	public void SetData() {
		testcaseName ="TC_004_EditLead";
		testdesc="Edit the leads";
	    author="Praveena";
		category="Regression";
	}
	
	@Test(groups="Sanity")
	public void editLeads() throws InterruptedException{
		

	WebElement leads = locateElement("Linktext", "Leads");
	click(leads);
	WebElement findLeads = locateElement("xpath", "//a[@href='/crmsfa/control/findLeads']");
	click(findLeads);
	WebElement firstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
	clearAndType(firstName, "Praveena");
	WebElement findLead= locateElement("xpath", "//button[text()='Find Leads']");
	click(findLead);
	Thread.sleep(3000);
	WebElement ele= driver.findElementByXPath("//table[@class ='x-grid3-row-table']");
	
	List<WebElement> elerow = ele.findElements(By.tagName("tr"));
	
	WebElement eleCols= locateElement("tagname", "td");
	
	click(driver.findElement(By.xpath("(//a[@class='linktext'])[4]")));
	
	
	
	
	String title = locateElement("xpath","//div[text()='View Lead']").getText();
	if(verifyTitle(title)==true) {
		System.out.println("The title is verified as View lead");
	}
	else {
		System.out.println("The title is not verified as View lead");
	}
WebElement edit = locateElement("xpath", "//a[text()='Edit']");
	
	click(edit);
	
	WebElement companyName = locateElement("id", "updateLeadForm_companyName");
	clearAndType(companyName, "Infy");
	
	String str = companyName.getText();
	WebElement update = locateElement("xpath", "//input[@value='Update']");
	
	click(update);
	
	String str1 = locateElement("xpath", "//span[@id='viewLead_companyName_sp']").getText();	

	if (str.contains(str1)) {
		System.out.println("The company name is updated");
	}
//Close the browser
	
	driver.close();
	
}
}