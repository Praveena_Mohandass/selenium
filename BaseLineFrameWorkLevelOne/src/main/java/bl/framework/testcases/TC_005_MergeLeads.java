package bl.framework.testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.ProjectDesign.Projectmethods;
import bl.framework.api.SeleniumBase;


public class TC_005_MergeLeads extends Projectmethods{

@BeforeTest
	
	public void SetData() {
		testcaseName ="TC_004_EditLead";
		testdesc="Edit the leads";
	    author="Praveena";
		category="Regression";
	}
	
	@Test
	public void mergeLeads() throws InterruptedException {
		
		WebElement leads = locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']");
		click(leads);
		WebElement mergeLead = locateElement("Linktext", "Merge Leads");
		click(mergeLead);
		WebElement fromLead = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		clickwithoutsnap(fromLead);
        switchToWindow(1);
        WebElement name = locateElement("name", "firstName");
        clearAndType(name, "ram");
      WebElement findLead=locateElement("xpath","//button[text()='Find Leads']");
      click(findLead);
      WebElement table = locateElement("xpath", "(//table[@class='x-grid3-row-table'])[1]");
      WebElement eleRow1 = locateElement("tagname", "tr");
		
		WebElement eleColumn1 = eleRow1.findElement(By.tagName("td"));
        Thread.sleep(2000);
		
		clickwithoutsnap(eleColumn1.findElement(By.xpath("(//a[@class='linktext'])[1]")));
		
		
		
		//driver.findElementByXPath("//a[@href='javascript:set_value('10029');']").click();
		
		switchToWindow(0);
		
		WebElement toLead = locateElement("xPath","(//img[@src='/images/fieldlookup.gif'])[2]");
		
		clickwithoutsnap(toLead);
		
		switchToWindow(1);
		
		
		WebElement firstname = locateElement("name","firstName");
		
		clearAndType(firstname, "ram");
		
		 click(findLead);
		
		WebElement table1 =  driver.findElementByXPath("(//table[@class='x-grid3-row-table'])[1]");
		
		WebElement eleRow = locateElement("tagname", "tr");
		
		WebElement eleColumn = eleRow.findElement(By.tagName("td"));
		
		Thread.sleep(2000);
		
		clickwithoutsnap(eleColumn.findElement(By.xpath("(//a[@class='linktext'])[1]")));
				
		//driver.findElementByXPath("//a[@href='javascript:set_value('10143');']").click();
		
		switchToWindow(0);
		
		clickwithoutsnap(locateElement("ClassName","buttonDangerous"));
		
		acceptAlert();
		
		click(locateElement("linkText","Find Leads"));
		
		clearAndType(locateElement("XPath","//input[@name='id']"),"10161");
		
		click(locateElement("xpath","//button[text()='Find Leads']"));
		
		System.out.println(driver.findElementByXPath("//div[text()='No records to display']").getText());
		
		
		
	}
}
