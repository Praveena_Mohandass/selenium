package bl.framework.testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class zoomCar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
     System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
     
     ChromeDriver driver = new ChromeDriver();
     
     driver.manage().window().maximize();
     
     driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
     
     //Launch the URL
     
     driver.get("https://www.zoomcar.com/chennai/");
     
     // Click on search
     
     driver.findElementByXPath("//a[@title='Start your wonderful journey']").click();
     
     // click on pick up point
     
     driver.findElementByXPath("//div[text()='Popular Pick-up points']/following-sibling::div[1]").click();
     
     driver.findElementByXPath("//button[@class='proceed']").click();
     
     // Select the date
     
    // driver.findElementByXPath("//div[@class='day picked ']").click();
     
     // To get the current date
     
     Date date = new Date();
     
     DateFormat sdf = new SimpleDateFormat("dd");
     
     String today = sdf.format(date);
     
     System.out.println(today);
     
    int tomorrow = Integer.parseInt(today)+1;
     
     System.out.println(tomorrow);
     
     driver.findElementByXPath("//div[contains(text(),"+tomorrow+")]").click();
     
     driver.findElementByXPath("//button[@class='proceed']").click();
     
     driver.findElementByXPath("//div[contains(text(),"+tomorrow+")]").click();
     
     driver.findElementByXPath("//button[@class='proceed']").click();
     
     driver.findElementByXPath("//button[text()='Done']").click();
     
     List<WebElement> carList= driver.findElementsByXPath("//div[@class='car-listing']");
     
     int results= carList.size();
     
     System.out.println("The number of results in the page is " +results);
     
//     driver.findElementByXPath(" //div[contains(text(),' Price: High to Low ')]").click();
//     
//     System.out.println("The Highest car price is " +driver.findElementByXPath("//div[@class='price']").getText());
//      
//     System.out.println("The Highest price brand name is " + driver.findElementByXPath("//div[@class='details']/h3").getText());
//     
   // sort the highest price using list
    		 
    List<WebElement> prices= driver.findElementsByXPath("//div[@class='price']");
   
     List<WebElement> carNames = driver.findElementsByXPath("//div[@class='details']/h3") ;
     
     int max =0;
     
     List<Integer> price= new ArrayList();
      
     for(WebElement ele : prices )  {
    	  
   		String priceValue =ele.getText().replaceAll("\\D","");
   		int  cost = Integer.parseInt(priceValue);
        price.add(cost);
        //System.out.println(priceValue);
    		 }
    	 
         for(int val : price) {
        	  if(val>max) {
        		 max=val;
        		}
        }
        // System.out.println("---"+max);
         
        System.out.println("The Highest price car brand is"+(driver.findElementByXPath("//div[contains(text(),'525')]/parent::div/parent::div/preceding-sibling::div[1]/h3").getText()));         
	
         driver.findElementByXPath("//button[text()='BOOK NOW']").click();
        
        driver.close();
	}
    	 
     
	}


